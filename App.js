import React, {Component} from 'react';
import { StatusBar } from 'react-native';
import {createAppContainer, createStackNavigator} from "react-navigation";

import LoginScreen from "./src/screens/LoginScreen";
import ChatsScreen from "./src/screens/ChatsScreen";
import ChatScreen from "./src/screens/ChatScreen";
import HomeScreen from "./src/screens/HomeScreen";

const AppStackNavigator = createStackNavigator({
  LoginScreen: LoginScreen,
  HomeScreen: HomeScreen,
  // ChatsScreen: ChatsScreen,
  ChatScreen: ChatScreen,
}, {
  headerMode: 'none',
  navigationOptions: {
    headerVisible: false,
  }
});

const AppStackContainer = createAppContainer(AppStackNavigator);

type Props = {};
export default class App extends Component<Props> {
  componentWillMount(): void {

  }

  componentDidMount(): void {
    StatusBar.setHidden(true);
    console.disableYellowBox = true;
  }

  render() {
    return (
        <AppStackContainer />
    );
  }
}