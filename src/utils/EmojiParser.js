import React, {Component} from 'react';
import {StyleSheet} from 'react-native';
import Emoji from "react-native-emoji";
import {Fonts} from "./Fonts";

export const parseMessage = (message) => {
    switch (message) {
        case ':)':
            return <Emoji name="blush" style={styles.emoji} />;
        case ':(':
            return <Emoji name="disappointed" style={styles.emoji} />;
        case ';)':
            return <Emoji name="wink" style={styles.emoji} />;
        case ':D':
            return <Emoji name="smiley" style={styles.emoji} />;
        case ':O':
            return <Emoji name="open_mouth" style={styles.emoji} />;
        case '<3':
            return <Emoji name="heart" style={styles.emoji} />;
        case ':P':
            return <Emoji name="stuck_out_tongue" style={styles.emoji} />;
        case ';(':
            return <Emoji name="cry" style={styles.emoji} />;
        case ':monkey:':
            return <Emoji name="monkey_face" style={styles.emoji} />;
        case ':dog:':
            return <Emoji name="dog" style={styles.emoji} />;
        case ':ok:':
            return <Emoji name="ok_hand" style={styles.emoji} />;
        case ':up:':
            return <Emoji name="+1" style={styles.emoji} />;
        case ':down:':
            return <Emoji name="-1" style={styles.emoji} />;
        default:
            return message + " ";
    }
};

const styles = StyleSheet.create({
    emoji: {
        fontSize: 16,
        marginHorizontal: 2
    }
});