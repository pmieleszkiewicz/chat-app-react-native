export const getUserFriends = (id, token) => {
    return fetch(`https://pm-laravel-chat-app.herokuapp.com/api/users/${id}/friends`, {
        method: 'get',
        headers: {
            'Accept': 'application/json',
            'Authorization': `Bearer ${token}`
        },
    });
};

export const getFilteredUsers = (id, token, search) => {
    return fetch(`https://pm-laravel-chat-app.herokuapp.com/api/users?search=${search}`, {
        method: 'get',
        headers: {
            'Accept': 'application/json',
            'Authorization': `Bearer ${token}`
        },
    });
};

export const createFriendship = (id, token, friend) => {
    const data = {
        ...friend,
    };

    return fetch(`https://pm-laravel-chat-app.herokuapp.com/api/users/${id}/friends`, {
        method: 'post',
        body: JSON.stringify(data),
        headers: {
            'Accept': 'application/json',
            'Authorization': `Bearer ${token}`,
            'Content-Type': 'application/json'
        },
    });
};

export const updateLocation = (id, token, location) => {

    return fetch(`https://pm-laravel-chat-app.herokuapp.com/api/users/${id}`, {
        method: 'patch',
        body: JSON.stringify({
            location: location
        }),
        headers: {
            'Accept': 'application/json',
            'Authorization': `Bearer ${token}`,
            'Content-Type': 'application/json'
        },
    });
};