export const login = (data) => {
    return fetch('https://pm-laravel-chat-app.herokuapp.com/api/auth/login', {
        method: 'post',
        body: JSON.stringify(data),
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        },
    });
};