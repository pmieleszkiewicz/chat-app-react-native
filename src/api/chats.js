export const getUserChats = (id, token) => {
    console.log(id, token);
    return fetch(`https://pm-laravel-chat-app.herokuapp.com/api/users/${id}/chats`, {
        method: 'get',
        headers: {
            'Accept': 'application/json',
            'Authorization': `Bearer ${token}`
        },
    });
};

export const getChatMessages = (id, page, token) => {
    return fetch(`https://pm-laravel-chat-app.herokuapp.com/api/chats/${id}/messages?page=${page}`, {
        method: 'get',
        headers: {
            'Accept': 'application/json',
            'Authorization': `Bearer ${token}`
        },
    });
};

export const sendMessage = (data, token) => {
    return fetch(`https://pm-laravel-chat-app.herokuapp.com/api/messages`, {
        method: 'post',
        headers: {
            'Accept': 'application/json',
            'Authorization': `Bearer ${token}`,
            'Content-Type': 'application/json'
        },
        body: JSON.stringify(data)
    });
};