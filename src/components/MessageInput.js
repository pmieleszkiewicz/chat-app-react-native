import React, {Component} from 'react';
import {View, StyleSheet, Text, TouchableOpacity} from 'react-native';
import {Icon, Input} from 'react-native-elements';

export default class MessageInput extends Component {
    constructor(props) {
        super(props);

        console.log(props)
        this.state = {
            message: ''
        }
    }

    sendMessage = () => {
        this.props.sendMessage(this.state.message);
        this.setState({
            message: ''
        });
        this.input.focus();
    };

    handleTyping = (text) => {
        if (this.state.message.length === 0 && text.length === 1) {
            this.props.handleTyping(true);
        } else if(text.length === 0) {
            this.props.handleTyping(false);

        }
        this.setState({
            message: text
        });
    }

    render() {
        return (
            <View
                style={{height: 60, flexDirection: 'row', alignItems: 'center', paddingRight: 15, paddingTop: 5, paddingBottom: 10, justifyContent: 'space-between'}}
            >
                <View style={{flex: 1}}>
                    <Input
                        inputContainerStyle={{borderBottomWidth: 0}}
                        inputStyle={styles.input}
                        placeholder='Write something...'
                        shake={true}
                        onChangeText={this.handleTyping}
                        value={this.state.message}
                        onSubmitEditing={this.sendMessage}
                        ref={(input) => { this.input = input; }}
                        blurOnSubmit={false}
                    />
                </View>
                <View>
                    <TouchableOpacity onPress={this.sendMessage} disabled={!this.state.message.length}>
                        <Icon
                            name="paper-plane"
                            size={24}
                            type="font-awesome"
                        />
                    </TouchableOpacity>
                </View>
            </View>
        );

    }
}

const styles = StyleSheet.create({
    input: {
        borderRadius: 25,
        backgroundColor: '#e8e8e8',
        paddingHorizontal: 15,
        fontSize: 16
    }
});

