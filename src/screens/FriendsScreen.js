import React, {Component} from 'react';
import {View, TouchableOpacity, Platform, StyleSheet, FlatList, ActivityIndicator, AsyncStorage} from 'react-native';
import {Header, Icon, Input, Text, ListItem, Button} from 'react-native-elements';
import {Fonts} from "../utils/Fonts";
import {createFriendship, getFilteredUsers, getUserFriends} from "../api/users";
import {calculateDistance} from "../utils/GpsDistanceCalculator";

export default class FriendsScreen extends Component {
    constructor(props) {
        super(props);

        this.state = {
            peopleFound: [],
            friends: [],
            isLoading: true,
            search: ''
        };
    }

    componentDidMount(): void {
        console.log('didMount');

        this.getCurrentLocation()
            .then(data => {
                this.location = JSON.parse(data);
                console.log(this.location)
            });

        this.getUser()
            .then(data => {
                this.user = JSON.parse(data);
                this.getUserFriends();

                this.props.navigation.addListener('didFocus', this.didFocus);
            });
    }

    getCurrentLocation = async () => {
        try {
            const value = await AsyncStorage.getItem('location');
            if (value !== null) {
                // console.log(value);
                return value;
            }
        } catch (error) {
            console.log(error)
        }
    };

    didFocus = () => {
        this.setState({
            search: ''
        });
        this.getUserFriends();
    };

    getUserFriends = () => {
        getUserFriends(this.user.id, this.user.token)
            .then(response => response.json())
            .then(data => {
                console.log(data.data)
                this.setState({
                    friends: data.data,
                    isLoading: false
                });
            });
    };

    // TODO: move this to separate service/file
    getUser = async () => {
        try {
            const value = await AsyncStorage.getItem('user');
            if (value !== null) {
                return value;
            }
        } catch (error) {
            console.log(error)
        }
    };

    addFriend = (friend) => {
        createFriendship(this.user.id, this.user.token, friend)
            .then(response => response.json())
            .then(data => {
                this.setState({
                    friends: [...this.state.friends, friend],
                    peopleFound: this.state.peopleFound.filter((item) => item.id !== friend.id),
                });
            });
    };

    keyExtractor = (item, index) => item.id.toString();

    renderItem = ({ item }) => {
        const subtitle = 'Last active: ' + item.last_active_date ? item.last_active_at : '';

        return (
            <ListItem
                title={`${item.first_name} ${item.last_name}`}
                titleStyle={{fontFamily: Fonts.NunitoBold, color: 'black'}}
                subtitle={subtitle}
                subtitleStyle={{fontFamily: Fonts.Nunito}}
                leftAvatar={{
                    source: item.avatar_url && {uri: item.avatar_url},
                    title: `${item.first_name[0]}${item.last_name[0]}`
                }}
                onPress={() => this.addFriend(item)}
                key={item.id}
                checkmark={() => <Icon name="user-plus" type="font-awesome" color="#111111" size={18} />}
            />
        )
    };

    renderFriendItem = ({ item }) => {

        let distance = null;

        console.log(item);

        if (item.last_location) {
            const location = item.last_location;
            const {latitude, longitude} = this.location;
            distance = calculateDistance(latitude, longitude, location.latitude, location.longitude);
        }
        // let subtitle = item.last_active_at ? `Last active: ${item.last_active_at}` : '';
        const subtitle = (distance ? `Last location: ${distance.toFixed(2)}km` : '');

        return (
            <ListItem
                title={`${item.first_name} ${item.last_name}`}
                titleStyle={{fontFamily: Fonts.NunitoBold, color: 'black'}}
                subtitle={subtitle}
                subtitleStyle={{fontFamily: Fonts.Nunito}}
                leftAvatar={{
                    source: item.avatar_url && {uri: item.avatar_url},
                    title: `${item.first_name[0]}${item.last_name[0]}`
                }}
                key={item.id}
            />
        )
    };

    handleSearchPerson = (text) => {
        if (text.length >= 3) {
            getFilteredUsers(this.user.id, this.user.token, text)
                .then(response => response.json())
                .then(data => {
                    this.setState({
                        peopleFound: data.data,
                    });
                });
        }
        this.setState({
            search: text
        });
    };

    render() {
        return (
            <View style={{flex: 1}}>
                <Header
                    containerStyle={{marginTop: Platform.OS === 'ios' ? 0 : - 24, paddingHorizontal: 15}}
                    leftComponent={{ icon: 'menu', color: '#fff' }}
                    centerComponent={{ text: 'My friends', style: { color: '#fff', fontFamily: Fonts.NunitoBold } }}
                    rightComponent={(
                        <TouchableOpacity onPress={this.logout}>
                            <Icon name="power-off" type="font-awesome" color="white" size={18} />
                        </TouchableOpacity>
                    )}
                />
                <View style={{marginVertical: 15}}>
                    <Input
                        inputContainerStyle={{borderBottomWidth: 0}}
                        inputStyle={styles.input}
                        placeholder='Find a person...'
                        shake={true}
                        onChangeText={this.handleSearchPerson}
                        value={this.state.search}
                        onSubmitEditing={this.sendMessage}
                        ref={(input) => { this.input = input; }}
                        blurOnSubmit={false}
                    />
                </View>
                {this.state.search.length >= 3 ? (
                    <View>
                        <FlatList
                            keyExtractor={this.keyExtractor}
                            data={this.state.peopleFound}
                            renderItem={this.renderItem}
                        />
                    </View>
                ) : <View></View>}

                <View>
                    {this.state.isLoading ? (<ActivityIndicator size="large" />) : (
                        <FlatList
                            ListHeaderComponent={() => <Text h4 style={{marginLeft: 10}}>My friends</Text>}
                            keyExtractor={this.keyExtractor}
                            data={this.state.friends}
                            renderItem={this.renderFriendItem}
                        />
                    )}
                </View>
            </View>
        );
    };
}

const styles = StyleSheet.create({
    input: {
        borderRadius: 25,
        backgroundColor: '#e8e8e8',
        paddingHorizontal: 15,
        fontSize: 16
    }
});

