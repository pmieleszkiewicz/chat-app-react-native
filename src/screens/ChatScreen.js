import React, {Component} from 'react';
import {View, StyleSheet, FlatList, Platform, SectionList, AsyncStorage, BackHandler, ActivityIndicator} from 'react-native';
import {Avatar, Button, Divider, Header, Input, ListItem, Text, Icon} from 'react-native-elements';
import {Fonts} from "../utils/Fonts";
import {getChatMessages, getUserChats, sendMessage} from "../api/chats";
import {getPusherConfig, subscribeChat} from "../services/pusher";
import Pusher from 'pusher-js/react-native';
import MessageInput from "../components/MessageInput";
import {parseMessage} from "../utils/EmojiParser";
import AnimatedEllipsis from 'react-native-animated-ellipsis';

export default class ChatScreen extends Component {
    constructor(props) {
        console.log('constructor');
        super(props);
        const {navigation} = this.props;

        this.chatId = navigation.getParam('id', 'None');
        this.chatName = navigation.getParam('name', '');

        this.state = {
            messages: [],
            isLoading: true,
            selectedMessageId: null,
            page: 1,
            lastPage: null,
            typingUsers: []
        }
    }

    componentWillMount(): void {
        this.getUser()
            .then(data => {
                this.user = JSON.parse(data);

                getChatMessages(this.chatId, this.state.page, this.user.token)
                    .then(response => response.json())
                    .then(data => {
                        console.log(data)
                        this.setState({
                            messages: data.data,
                            isLoading: false,
                            lastPage: data.last_page
                        });

                        Pusher.logToConsole = true;
                        const pusherConfig = getPusherConfig(this.user.token);
                        this.pusher = new Pusher(pusherConfig.key, pusherConfig);

                        this.pusher.connection.bind('state_change', function (states) {
                            console.log(states.current)
                        });

                        this.chatChannel = this.pusher.subscribe(`private-chats.${this.chatId}`)
                            .bind('App\\Events\\NewMessage', (data) => {
                                const message = data.message;
                                if (message.user.id !== this.user.id) {
                                    this.updateMessages(data);
                                }
                            })
                            .bind('client-UserStoppedTyping', (data) => {
                                console.log(data)
                                if (data.user.id === this.user.id)
                                    return;

                                this.setState((prevState, props) => ({
                                    messages: prevState.messages.filter((item) => !(item.id === 0 && item.user.id === data.user.id))
                                }));

                            })
                            .bind('client-UserTyping', ({user}) => {
                                if (user.id === this.user.id)
                                    return;

                                const message = {
                                    id: 0,
                                    content: '...',
                                    user: {...user},
                                    created_at: ''
                                };

                                this.setState((prevState, props) => ({
                                    messages: [message, ...prevState.messages]
                                }));
                            });

                        this.chatChannel.bind('pusher:subscription_error', function(status) {
                            console.log(status)
                        });

                        this.chatChannel.bind('pusher:subscription_succeeded', function() {
                            console.log('Sukces')
                        });
                    });
            });
    }

    componentWillUnmount(): void {
        this.chatChannel.unsubscribe(`private-chats.${this.chatId}`);
    }

    updateMessages = ({message}) => {
        this.setState({
            messages: [message, ...this.state.messages.filter((item) => !(item.id === 0 && item.user.id === message.user.id))]
        })
    };

    // TODO: move this to separate service/file
    getUser = async () => {
        try {
            const value = await AsyncStorage.getItem('user');
            if (value !== null) {
                return value;
            }
        } catch (error) {
            console.log(error)
        }
    };

    showMessageDate = (id) => {
        if (this.state.selectedMessageId === id) {
            id = null;
        }
        this.setState({
            selectedMessageId: id
        });
    };

    sendMessage = (message) => {
        sendMessage({content: message, chat_id: this.chatId}, this.user.token)
            .then((response) => response.json())
            .then((data) => {
                const {user} = this;

                this.setState({
                    messages: [data.data, ...this.state.messages]
                }, () => {
                    console.log(this.state.messages)
                });
            })
    };

    loadMoreMessages = () => {
        console.log('LOADING MORE');
        const page = this.state.page + 1;

        if (this.state.lastPage === this.state.page) {
            return;
        }

        getChatMessages(this.chatId, page, this.user.token)
            .then(response => response.json())
            .then(data => {
                this.setState({
                    messages: [...this.state.messages, ...data.data],
                    isLoading: false
                });
            });

        this.setState({
            page: page
        });
    };

    keyExtractor = (item, index) => {
        return item.id.toString()
    };

    renderItem = ({item, index}) => {
        const isMine = this.user.id === item.user.id;
        const isNextTheSameUsersMsg = item.user.id === (this.state.messages[index + 1] ? this.state.messages[index + 1].user.id : -1);

        const messages = item.content.split(' ');

        const message = messages.map(parseMessage);
        return (
            <View>
                <View
                    style={{flex: 1, marginVertical: 5, flexDirection: 'row', justifyContent: isMine ? 'flex-end' : 'flex-start'}}
                >
                    {(!isMine && !isNextTheSameUsersMsg || item.id === 0) ? (
                        <Avatar
                            rounded
                            title={`${item.user.first_name[0]}${item.user.last_name[0]}`}
                        />) : (
                        <View></View>
                    )}
                    {(isNextTheSameUsersMsg && item.id !== 0) ? (
                        <View style={{width: 35}}></View>) : (<View></View>)}

                    {item.id === 0 ? <View style={{marginLeft: 10}}><AnimatedEllipsis animationDelay={200} /></View> : (<Text
                            style={isMine ? styles.myMessage: styles.friendsMessage}
                            onPress={() => this.showMessageDate(item.id)}
                        >
                            {message}
                        </Text>
                    )}
                </View>

                {(item.id === this.state.selectedMessageId) ? (
                    <View style={{paddingRight: (isMine) ? 10 : 0, paddingLeft: (!isMine) ? 45 : 0}}>
                        <Text style={{textAlign: (isMine) ? 'right' : 'left', fontSize: 12}}>{item.created_at}</Text>
                    </View>
                ) : (<View></View>)}

            </View>
        )
    };

    handleTyping = (isTyping) => {
        const {user} = this;
        if (isTyping) {
            this.chatChannel.trigger('client-UserTyping', {user: {id: user.id, first_name: user.first_name, last_name: user.last_name, gender: user.gender}});
        } else {
            this.chatChannel.trigger('client-UserStoppedTyping', {user: {id: user.id, first_name: user.first_name, last_name: user.last_name, gender: user.gender}});
        }
    };

    render() {
        const {messages, isLoading} = this.state;

        return (
            <View style={{display: 'flex', flex: 1}}>
                <View>
                    <Header
                        containerStyle={{marginTop: Platform.OS === 'ios' ? 0 : - 24, paddingLeft: 15}}
                        leftComponent={{ icon: 'menu', color: '#fff' }}
                        centerComponent={{ text: this.chatName, style: { color: '#fff', fontFamily: Fonts.NunitoBold } }}
                    />
                </View>

                <View style={styles.feed}>
                    {isLoading ? (<ActivityIndicator size="large" />) : (
                        <FlatList
                            inverted
                            keyExtractor={this.keyExtractor}
                            data={messages}
                            renderItem={this.renderItem}
                            showsVerticalScrollIndicator={false}
                            extraData={this.state}
                            onEndReached={this.loadMoreMessages}
                            onEndReachedThreshold={0.5}
                        />
                    )}
                </View>
                <MessageInput handleTyping={this.handleTyping} sendMessage={this.sendMessage} />
            </View>
        );
    };
}

const styles = StyleSheet.create({
    feed: {
        flex: 5,
        paddingHorizontal: 15,
        // paddingBottom: 5,
        justifyContent: 'center',
    },
    myMessage: {
        marginRight: 10,
        paddingVertical: 10,
        paddingHorizontal: 10,
        fontSize: 16,
        textAlign: 'right',
        fontFamily: Fonts.Nunito,
        backgroundColor: '#77b4f7',
        borderRadius: 20,
        color: 'white',
        maxWidth: '60%',
    },
    friendsMessage: {
        borderRadius: 20,
        marginLeft: 10,
        color: 'black',
        paddingVertical: 10,
        paddingHorizontal: 10,
        maxWidth: '60%',
        fontSize: 16,
        textAlign: 'left',
        fontFamily: Fonts.Nunito,
        backgroundColor: 'lightgrey'
    },
    input: {
        borderRadius: 25,
        backgroundColor: '#e8e8e8',
        paddingHorizontal: 15,
        fontSize: 16
    }
});

