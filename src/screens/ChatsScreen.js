import React, {Component} from 'react';
import {View, StyleSheet, FlatList, Platform, SectionList, AsyncStorage, BackHandler, TouchableOpacity, ActivityIndicator} from 'react-native';
import {Button, Divider, Header, Icon, Input, ListItem, Text} from 'react-native-elements';
import {Fonts} from "../utils/Fonts";
import {getUserChats} from "../api/chats";
import {parseMessage} from "../utils/EmojiParser";
import {updateLocation} from "../api/users";

export default class ChatsScreen extends Component {
    constructor(props) {
        super(props);

        this.state = {
            chats: [],
            isLoading: true
        }
    }

    componentDidMount(): void {
        console.log('nav')
        navigator.geolocation.getCurrentPosition(
            position => {
                const location = position.coords;

                this.setState({ location }, () => {
                    console.log(this.state.location)
                });

                updateLocation(this.user.id, this.user.token, location)
                    .then(response => response.json())
                    .then(data => {
                        AsyncStorage.setItem('location', JSON.stringify(data.data.last_location));
                    });
            }, error => Alert.alert(error.message), { enableHighAccuracy: true, timeout: 20000, maximumAge: 1000 });

    }

    didFocus = () => {
        this.getUserChats();
    };

    componentWillUnmount() {

    }

    componentWillMount(): void {
        // BackHandler.addEventListener('hardwareBackPress', true);

        this.getUser()
            .then(data => {
                this.user = JSON.parse(data);
                console.log(this.user)

                this.setState({
                    isLoading: true
                });

                this.getUserChats();
                this.props.navigation.addListener('didFocus', this.didFocus);
            });
    }

    getUserChats = () => {
        getUserChats(this.user.id, this.user.token)
            .then(response => response.json())
            .then(data => {
                this.setState({
                    chats: data.data,
                    isLoading: false
                });
            });
    };

    // TODO: move this to separate service/file
    getUser = async () => {
        try {
            const value = await AsyncStorage.getItem('user');
            if (value !== null) {
                // console.log(value);
                return value;
            }
        } catch (error) {
            console.log(error)
        }
    };

    keyExtractor = (item, index) => item.id.toString();

    renderItem = ({ item }) => {
        const {last_message} = item;
        let first_name = '';
        let message = 'No messages';

        if (last_message) {
            first_name = last_message.user.first_name;
            message = last_message.content.length > 30 ? last_message.content.substring(27) + '...'
                : last_message.content;
        }

        message = message.split(' ').map(parseMessage);
        // console.log(message);
        const subtitle = (
            <View style={{flexDirection: 'row'}}>
                <Text>{first_name}: </Text>
                <Text>{message}</Text>
            </View>
        );

        return (
            <ListItem
                bottomDivider={true}
                title={item.name}
                titleStyle={{fontFamily: Fonts.NunitoBold, color: 'black'}}
                subtitle={subtitle}
                subtitleStyle={{fontFamily: Fonts.Nunito}}
                leftAvatar={{
                    source: item.avatar_url && {uri: item.avatar_url},
                    title: item.name[0]
                }}
                onPress={() => this.props.navigation.navigate('ChatScreen', {id: item.id, name: item.name})}
                key={item}
            />
        )
    };

    removeUser = async () => {
        try {
            await AsyncStorage.removeItem('user');
            return true;
        }
        catch(exception) {
            console.log(exception);
            return false;
        }
    };

    logout = () => {
        this.removeUser()
            .then(() => {
                this.props.navigation.replace('LoginScreen');
            })
    };

    render() {
        const {isLoading} = this.state;
        return (
            <View>
                <Header
                    containerStyle={{marginTop: Platform.OS === 'ios' ? 0 : - 24, paddingHorizontal: 15}}
                    leftComponent={{ icon: 'menu', color: '#fff' }}
                    centerComponent={{ text: this.user ? `${this.user.first_name} ${this.user.last_name}` : '', style: { color: '#fff', fontFamily: Fonts.NunitoBold } }}
                    rightComponent={(
                        <TouchableOpacity onPress={this.logout}>
                            <Icon name="power-off" type="font-awesome" color="white" size={18} />
                        </TouchableOpacity>
                    )}
                />
                <View>
                    {this.state.isLoading ? (
                        <ActivityIndicator size="large" color="#0000ff" />
                    ) : (
                        <FlatList
                            keyExtractor={this.keyExtractor}
                            data={this.state.chats}
                            renderItem={this.renderItem}
                        />
                    )}
                </View>
            </View>
        );
    };
}

