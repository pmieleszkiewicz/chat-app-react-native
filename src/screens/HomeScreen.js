import React, {Component} from 'react';
import {Icon} from 'react-native-elements';

import {createBottomTabNavigator} from "react-navigation";

import ChatsScreen from "./ChatsScreen";
import FriendsScreen from "./FriendsScreen";

export default createBottomTabNavigator({
    ChatsScreen: {
        screen: ChatsScreen,
        navigationOptions: {
            tabBarLabel: 'Chats',
            tabBarIcon: ({tintColor}) => (
                <Icon name="comments" type="font-awesome" color={tintColor} size={18} />
            )
        }
    },
    FriendsScreen: {
        screen: FriendsScreen,
        navigationOptions: {
            tabBarLabel: 'Friends',
            tabBarIcon: ({tintColor}) => (
                <Icon name="users" type="font-awesome" color={tintColor} size={18} />
            )
        }
    }
});

