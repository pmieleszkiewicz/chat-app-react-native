import React, {Component} from 'react';
import {View, Text, StyleSheet, KeyboardAvoidingView} from 'react-native';
import { Button, Header, Input } from 'react-native-elements';
import Icon from 'react-native-vector-icons/FontAwesome';
import {Fonts} from "../utils/Fonts";
import Pusher from 'pusher-js/react-native';
import Echo from '../services/laravel-echo';
import {login} from "../api/auth";
import {AsyncStorage} from "react-native";

const token = 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOlwvXC9wbS1sYXJhdmVsLWNoYXQtYXBwLmhlcm9rdWFwcC5jb21cL2FwaVwvYXV0aFwvbG9naW4iLCJpYXQiOjE1NTc5NTM3NzUsImV4cCI6MTU1Nzk1NzM3NSwibmJmIjoxNTU3OTUzNzc1LCJqdGkiOiJwQnZud05HOU55ZFpjRTVCIiwic3ViIjoxLCJwcnYiOiI4N2UwYWYxZWY5ZmQxNTgxMmZkZWM5NzE1M2ExNGUwYjA0NzU0NmFhIn0.GKGyPYMCT61Q-w62DpYWQCR4JoVBxa7U0uGJiHQmhwY';

const pusherConfig = {
    "appId":"780424",
    "key":"3cd900b8421792998b7c",
    "secret":"16663eb5ef26c71120be",
    "cluster":"eu",
    "encrypted":true,
    "restServer":"https://pm-laravel-chat-app.herokuapp.com",
    'authEndpoint':'https://pm-laravel-chat-app.herokuapp.com/broadcasting/auth',
    'wsHost': 'https://pm-laravel-chat-app.herokuapp.com',
    'auth': {
        headers: {
            'Authorization': `Bearer ${token}`
        }
    }
};

export default class LoginScreen extends Component {
    constructor(props) {
        super(props);

        this.state = {
            email: 'pawelmieleszkiewicz@gmail.com',
            password: 'Pawel!23',
            error: '',
            loading: false,
            screenLoaded: false
        };
    }

    handleInputChange = (text, key) => {
        this.setState({
            [key]: text
        });
    };

    login = () => {
        this.setState({
            loading: true
        });
        const {email, password} = this.state;
        const credentials = {
            email: email,
            password: password
        };

        login(credentials)
            .then(response => {
                console.log(response);
                if (response.status === 401) {
                    this.setState({
                        error: 'Invalid credentials'
                    });
                }
                return response.json();
            }).then(data => {
                console.log(data);
                this.setState({
                    error: data.token,
                    loading: false
                });
                this.storeToken(data);
                this.props.navigation.replace('HomeScreen');
            });
    };

    storeToken = async (data) => {
        const {token} = data;
        const user = {...data.user, token};

        try {
            await AsyncStorage.setItem('user', JSON.stringify(user));
        } catch (error) {
            console.log(error);
        }
    };

    // TODO: move this to separate service/file
    getUser = async () => {
        try {
            const value = await AsyncStorage.getItem('user');
            // console.log(value)
            // console.log(value);
            return value;
        } catch (error) {
            console.log(error)
        }
    };

    componentWillMount() {
        this.getUser()
            .then((data) => {
                if (data !== null) {
                    this.props.navigation.replace('HomeScreen');
                } else {
                    this.setState({
                        screenLoaded: true
                    });
                }
            });
    }

    render() {
        return this.state.screenLoaded ? (
            <KeyboardAvoidingView behavior="padding" style={styles.container}>
                <View>
                    <Text style={styles.welcome}>Welcome to Chat App</Text>
                    {/*<Text style={styles.welcome}>{this.state.error}</Text>*/}
                </View>
                <View style={styles.input}>
                    <Input
                        value={this.state.email}
                        onChangeText={(text) => this.handleInputChange(text, 'email')}
                        onSubmitEditing={() => {this.passwordInput.focus()}}
                        placeholder='Email'
                        returnKeyType="next"
                        blurOnSubmit={false}
                        shake={true}
                        // value="marynarz@gmail.com"
                    />
                </View>
                <View style={{...styles.input, marginBottom: 30}}>
                    <Input
                        value={this.state.password}
                        onChangeText={(text) => this.handleInputChange(text, 'password')}
                        ref={(input) => { this.passwordInput = input }}
                        secureTextEntry
                        placeholder='Password'
                        // value="marynarz"
                    />
                </View>
                <View style={styles.button}>
                    <Button
                        title="Login"
                        onPress={this.login}
                        disabled={false}
                        loading={this.state.loading}
                    />
                </View>
                <View style={styles.button}>
                    <Button
                        title="Sign up"
                        type="clear"
                    />
                </View>
            </KeyboardAvoidingView>
        ) : (
            <View></View>
        );
    };
}

const styles = StyleSheet.create({
    container: {
        paddingHorizontal: 25,
        paddingVertical: 25,
        flex: 1,
        justifyContent: 'center',
        fontFamily: 'monospace'
    },
    welcome: {
        fontSize: 28,
        textAlign: 'center',
        marginBottom: 50,
        fontFamily: Fonts.NunitoBold
    },
    input: {
        textAlign: 'center',
        marginBottom: 20,
    },
    button: {
        marginBottom: 5
    }
});