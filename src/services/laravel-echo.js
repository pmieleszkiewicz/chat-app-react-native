import Echo from 'laravel-echo/dist/echo';
import Pusher from 'pusher-js/react-native';



export default class EchoBridge {
    constructor(token) {
        this.token = token;
        this.options = {
            broadcaster: 'pusher',
            key: '3cd900b8421792998b7c',
            cluster: 'eu',
            authEndpoint: 'https://pm-laravel-chat-app.herokuapp.com/broadcasting/auth',
            restServer: "https://pm-laravel-chat-app.herokuapp.com",
            encrypted: true,
            wsHost: 'https://pm-laravel-chat-app.herokuapp.com',
            client: new Pusher('3cd900b8421792998b7c', {cluster: 'eu'}),
            auth: {
                headers: {
                    Authorization: `Bearer ${this.token}`
                }
            }
        };
    }

    connect() {
        console.log(this.options.auth.headers.Authorization)
        return new Echo(this.options);
    }
}
