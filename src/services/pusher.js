import Pusher from 'pusher-js/react-native';

let pusherConfig = {
    "appId":"780424",
    "key":"3cd900b8421792998b7c",
    "secret":"16663eb5ef26c71120be",
    "cluster":"eu",
    "encrypted":true,
    "restServer":"https://pm-laravel-chat-app.herokuapp.com",
    'authEndpoint':'https://pm-laravel-chat-app.herokuapp.com/broadcasting/auth',
    'wsHost': 'https://pm-laravel-chat-app.herokuapp.com',
    'auth': {
        headers: {
            'Authorization': ''
        }
    }
};

export const getPusherConfig = (token) => {
    pusherConfig.auth.headers.Authorization = `Bearer ${token}`;
    return pusherConfig;
};

export const getPusher = () => {

    pusherConfig.auth.headers.Authorization = `Bearer ${token}`;
    Pusher.logToConsole = true;
    const pusher = new Pusher(pusherConfig.key, pusherConfig);

    pusher.connection.bind('state_change', function (states) {
        console.log(states.current)
    });

    // const chatChannel = this.pusher.subscribe('private-chats.1')
    //     .bind('NewMessage', (data) => {
    //         console.log(data);
    // });

    const chatChannel = this.pusher.subscribe(`private-chats.${id}`);

    // chatChannel.bind('pusher:subscription_error', function(status) {
    //     console.log(status)
    // });
    //
    // chatChannel.bind('pusher:subscription_succeeded', function() {
    //     console.log('Sukces')
    // });

    return chatChannel;
};